from __future__ import print_function

import pandas as pd
from pandas import Series
import matplotlib as mp
import numpy as np

print("Version")
print(pd.__version__)

city_names = pd.Series(['San Francisco', 'San Jose', 'Sacramento', 'Los Angeles'])
population: Series = pd.Series([852469, 1015785, 485199, 100])

pd.DataFrame({'City name': city_names, 'Population': population})

california_housing_dataframe = \
    pd.read_csv("https://download.mlcc.google.com/mledu-datasets/california_housing_train.csv", sep=",")

print("Describe")
print(california_housing_dataframe.describe())
print("Head")
print(california_housing_dataframe.head())
print("Hist")
print(california_housing_dataframe.hist('housing_median_age'))

cities = pd.DataFrame({'City name': city_names, 'Population': population})
print("City types")
print(type(cities['City name']))
print("All city names")
print(cities['City name'])
print("Second city name type")
print(type(cities['City name'][1]))
print("Second city name")
print(cities['City name'][1])
print("From first to third city type")
print(type(cities[0:2]))
print("From first to third city names")
print(cities[0:3])
print("All population")
print(cities['Population'])

print("Operations")
print(population / 1000)

print("Numpy")
print(np.log(population))
print(population.apply(lambda val: val > 1000000))

cities['Area square miles'] = pd.Series([46.87, 176.53, 97.92, 76.98])
cities['Population density'] = cities['Population'] / cities['Area square miles']
print(cities)

print(type(cities['City name'][0]))
cities['Is wide and has saint name'] = (cities['Area square miles'] > 50) & cities['City name'].apply(lambda name: name.startswith('San'))

print("Cities - retake")
print(cities)

print(city_names.index)
print(cities.index)
print(cities.reindex([2, 0, 1]))
print(cities.reindex(np.random.permutation(cities.index)))
print(cities.reindex([0, 4, 5, 2]))
