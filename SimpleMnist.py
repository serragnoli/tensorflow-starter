from __future__ import print_function
import tensorflow as tf

# Import data
from tensorflow.examples.tutorials.mnist import input_data

mnist = input_data.read_data_sets('/tmp/data', one_hot=True)

# Hyperparameters
learning_rate = 0.001
training_iters = 200000
batch_size = 128
display_step = 10

# Network Paramenters
# 28 x 28 image = 28^2
n_input = 784
n_classes = 10
dropout = 0.75

# Create definitions
x = tf.placeholder(tf.float32, [None, n_input])
y = tf.placeholder(tf.float32, [None, n_classes])
keep_prob = tf.placeholder(tf.float32)


def conv2d(x, W, b, strides=1):
    x = tf.nn.conv2d(x, W, strides=[1, strides, strides, 1])
    x = tf.nn.bias_add(x, b)
    return tf.nn.relu(x)


def maxpool2d(x, k=2):
    return tf.nn.max_pool(x, ksize=[1, k, k, 1], strides=[1, k, k, 1], padding='SAME')


# Create model
def conv_net(x, weights, biases, dropout):
    # reshape inputdata
    x = tf.reshape(x, shape=[-1, 28. 28, 1])

    # convolutional layer
    conv1 = conv2d(x, weights['wc1'], biases['bc1'])
    # max pooling layer
    conv1 = maxpool2d(conv1, weights['wc2'], biases['bc2'])

    conv2 = conv2d(conv1, weights['wc2'], biases['bc2'])
    conv2 = maxpool2d(conv2, k=2)

    fc1 = tf.reshape(conv2, [-1, weights['wd1'].get_shape().as_list()])
    fc1 = tf.add(tf.matmul(fc1, weights['wd1'], biases['bd1']))
    fc1 = tf.nn.relu(fc1)
    # apply dropout
    fc1 = tf.nn.dropout(fc1, dropout)

    # output, class prediction
    out = tf.add(tf.matmul(fc1, weights['out'], biases['out']))
    return out


# Create weights
weights = {
    'wc1': tf.Variable(tf.random_normal([5, 5, 1, 32])),
    'wc2': tf.Variable(tf.random_normal([5, 5, 32, 64])),
    'wd1: tf.Variable(tf.random_normal(['''])),
}
