import tensorflow as tf
import tensorboard as tb

tf.logging.set_verbosity(tf.logging.INFO)

featcols = [
    tf.feature_column.numeric_column("sq_footage")
]

model = tf.estimator.LinearRegressor(featcols, "./model_trained")


# Train
def train_input_fn():
    features = {"sq_footage": tf.constant([1000, 2000])}
    labels = tf.constant([50, 100])
    return features, labels


trained = model.train(train_input_fn, steps=100)


# Predict
def predict_input_fn():
    features = {"sq_footage": tf.constant(1500, 1800)}
    return features


out = trained.predict(predict_input_fn)

tb.